import java.util.Scanner;
public class Sudoku {
	static int contadorSupremo = 0;  //Nos dir� los n�meros obtenidos mediante el m�todo Greedy
	static int[][]sudoku=new int[9][9];
	
	
	public static int[][] leerSudoku() {
        Scanner s = new Scanner(System.in);
        //matriz[fila][columna]
        int[][] matriz = new int[9][9];
        //como el input siempre es igual, leemos 9 lineas
        for (int i = 0; i < 9; i++) {
        	//separamos cada linea en un arreglo de strings, con espacio como separador
            String[] linea = s.nextLine().split(" ");
            //leemos el arreglo de lineas
            for (int j = 0; j < linea.length; j++) {
                matriz[i][j] = Integer.parseInt(linea[j]); //para pasar de String a int
            }
        }
        s.close();
        return matriz;
    }
	
	
	public static int[][][] crearMatrizCandidatos(int[][] matriz){  //crea matriz de candidatos para el sudoku
		int[][][] candidatos = new int[9][9][9];
		for(int i=0; i<9;i++) {
			for(int j=0; j<9;j++) {
					if (matriz[i][j] == 0) {
						for (int k=0; k<9;k++) {
							candidatos[i][j][k]=k+1;
						}
					}
			}
		}
		return candidatos;
	}
	
	
	public static int[] reemplazarNum(int num, int[] array){   //toma un n�mero y un array, busca el n�mero en el array y si est� lo reemplaza por 0
		for(int i=0; i<9; i++) {
			if (array[i]==num) {
				array[i]=0;
			}
		}
		return array;
	}

	
	public static int[][][] eliminarCandidatosFila(int[][] matriz, int[][][] candidatos,int fila, int columna){ //elimina candidatos en las filas
		for(int f=0; f<9; f++) {
			int numero=matriz[fila][f];
			candidatos[fila][columna] = reemplazarNum(numero,candidatos[fila][columna]);
				}
		return candidatos;
	}

	
	public static int[][][] eliminarCandidatosColumna(int[][] matriz, int[][][] candidatos,int fila, int columna){ //elimina candidatos en las columnas
		for(int f=0; f<9; f++) {
			int numero=matriz[f][columna];
			candidatos[fila][columna] = reemplazarNum(numero,candidatos[fila][columna]);
				}
		return candidatos;
	}

	
	public static int[][][] eliminarCandidatosCuadrante(int[][] matriz, int[][][] candidatos,int fila, int columna){ //elimina candidatos en los cuadrantes
		int cuadrantex = fila/3;
		int cuadrantey = columna/3;
		for(int i= cuadrantex*3; i<(cuadrantex*3)+3; i++) {
			for(int j= cuadrantey*3; j<(cuadrantey*3)+3; j++) {
				int numero=matriz[i][j];
				candidatos[fila][columna]=reemplazarNum(numero,candidatos[fila][columna]);
			}
		}
		return candidatos;
	}
	
	
	public static int[][][] eliminarCandidatosSudoku(int[][] matriz, int[][][] candidatos){  //elimina candidatos en todo el sudoku
		for(int i=0; i<9; i++) {
			for(int j=0; j<9; j++) {
				candidatos = eliminarCandidatosFila(matriz,candidatos,i,j);
				candidatos = eliminarCandidatosColumna(matriz,candidatos,i,j);
				candidatos = eliminarCandidatosCuadrante(matriz,candidatos,i,j);
				}
		}
		return candidatos;
	}
	
	
	public static int[][][] agregarCandidatosPropuestos(int[][] matriz, int[][][] candidatos){ // Busca las listas de candidatos que solamente tienen uno y los pone en el sudoku
		for(int i=0; i<9; i++) {
			for(int j=0; j<9; j++) {
				int contador = 0;
				int num = 0;
				for(int k=0; k<9; k++) {
					if(candidatos[i][j][k]!=0) {
						contador++;
						num = candidatos[i][j][k];
					}
				}
				if(contador==1) {			
					matriz[i][j]=num;  
					contadorSupremo++; 	//Un n�mero para la cuenta del Greedy
					candidatos[i][j]= reemplazarNum(num,candidatos[i][j]);
				}
			}
		}
		return candidatos;
	}
	
	
	public static int[][][] metodoGreedy(int[][] matriz, int[][][]candidatos){  //Resuelve el sudoku mediante el m�todo greedy
		int contadorChiquito= -1;
		while(contadorChiquito<contadorSupremo) {
			contadorChiquito = contadorSupremo;
			candidatos= eliminarCandidatosSudoku(matriz,candidatos);
			candidatos = agregarCandidatosPropuestos(matriz, candidatos);
		}
		return candidatos;
	}
		
	
	public static boolean filaValida(int[][]matriz) {	//Busca errores en las filas
		for (int i=0;i<9;i++) {
			int[] whiteList = new int[9];
			for(int j=0;j<9;j++) {
				if (matriz[i][j]!=0) {
					for(int k =0;k<9;k++) {
						if (matriz[i][j]==whiteList[k]) {
							return false;					//si encuentra uno igual retorna Falso
						}									
					}
					whiteList[j]=matriz[i][j]; //contrario lo agrega a la whitelist
				}
			}
		}
		return true;	//No encontr� ning�n repetido
	}
	
	
	public static boolean columnaValida(int[][]matriz){  //Busca errores en las columnas
		for (int j=0;j<9;j++) {
			int[] whiteList = new int[9];
			for(int i=0;i<9;i++) {
				if (matriz[i][j]!=0) {
					for(int k =0;k<9;k++) {
						if (matriz[i][j]==whiteList[k]) {
							return false;					//si encuentra uno igual retorna Falso
						}									
					}
					whiteList[i]=matriz[i][j]; //contrario lo agrega a la whitelist
				}
			}
		}
		return true; //No encontr� ning�n repetido
		
	}
	
	
	public static boolean cuadranteValido(int[][]matriz) {  //Busca errores en los cuadrantes
		for(int x=0;x<3;x++) {
			for(int y=0;y<3;y++) {
				int[] whiteList = new int[9];
				int num = -1;								// Para poder ubicarlos en la whitelist
				for(int i=x*3;i<(x*3)+3;i++) {
					for(int j=y*3;j<(y*3)+3;j++) {
						num++;
						if(matriz[i][j]!=0) {
							for(int k =0;k<9;k++) {
								if (matriz[i][j]==whiteList[k]) {
									return false;					//si encuentra uno igual retorna Falso
								}
							}
							whiteList[num]=matriz[i][j];
						}
					}
					
				}
			}
		}
		return true;		//No encontr� ning�n repetido
	}
	
	
	public static boolean sudokuValido(int[][]matriz) {    //Indica si un sudoku no tiene errores
		return (filaValida(matriz) && columnaValida(matriz) && cuadranteValido(matriz)); 
	}
	
	
	public static boolean sudokuLleno(int[][]matriz) { 		//Revisa si un sudoku est� lleno
		for(int i =0; i<9;i++) {
			for(int j=0; j<9;j++) {
				if(matriz[i][j]==0) {
					return false;
				}
			}
		}
		return true;
	}
	
	
	public static boolean backTracking(int[][] matriz, int[][][]candidatos) {  //Se encarga del Backtracking		
		for(int i=0; i<9;i++) {							//itera en todo el sudoku
			for(int j=0;j<9;j++) {
				if(matriz[i][j]==0) {				//Se encarga de trabajar solamente con los casilleros vac�os
					for(int k=0; k<9;k++) {
						int num=candidatos[i][j][k];		
						if(num!=0) {				//Se asegura de poner un candidato y no dejar vac�o por error 
							matriz[i][j]=num;
							candidatos[i][j]=reemplazarNum(num,candidatos[i][j]);
							if(sudokuValido(matriz)) {			//Si es v�lido contin�a 
								if(backTracking(matriz,candidatos)) {  //Si el siguiente backtracking es correcto
									return true;					//Se genera una cadena de true
								}else {
									matriz[i][j]=0;					//Si falla se retracta y sigue
									candidatos=crearMatrizCandidatos(matriz);
									candidatos=eliminarCandidatosSudoku(matriz,candidatos);
								}
							}else {							//Si no es v�lido se retracta y sigue con otro n�mero
								matriz[i][j]=0;
								candidatos=crearMatrizCandidatos(matriz);
								candidatos=eliminarCandidatosSudoku(matriz,candidatos);
							}
						}
					}
					return false;		//Si ninguno es v�lido se equivoc� al poner un n�mero
				}
				
			}
		}
		return true;
	}
	
	
	public static void imprimirSudoku(int[][]matriz) {  //imprime el sudoku
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j<9; j++) {
				System.out.print(matriz[i][j]+" ");
			}
			System.out.println();
		}
	}
	
	
	public static void main(String[] args) {  //Funci�n principal, self explanatory
		sudoku= leerSudoku();									//Inicializaci�n
		int[][][]candidatos = crearMatrizCandidatos(sudoku);	//Inicializaci�n
		
		candidatos=metodoGreedy(sudoku,candidatos);				//m�todo Greedy
		
		backTracking(sudoku,candidatos);						//m�todo BackTracking
		
		imprimirSudoku(sudoku);								//output
		System.out.println(contadorSupremo); 				//output
	}

}
