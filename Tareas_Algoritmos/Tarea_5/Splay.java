public class Splay {

	public static Nodo insertar(Nodo a, double x) { //Llama con true para saber qui�n es la verdadera raiz
		return insertar2(a, x, true);
	}

	public static Nodo insertar2( Nodo a, double x, boolean raiz) { //Basado en insertar de AVL
		if( a instanceof NodoExt ) {
			return new NodoInt( Nodo.nulo, x, Nodo.nulo );
		}
		
		NodoInt b = (NodoInt) a;
		
		if( x < b.info ) {
			NodoInt retorno = new NodoInt(insertar2(b.izq,x,false),b.info,b.der);
			return revisar(retorno, x, raiz);
		}
		else if( x > b.info ) {
			NodoInt retorno = new NodoInt(b.izq,b.info,insertar2(b.der,x, false));
			return revisar(retorno, x, raiz);
		}
		else
			return b; // ignoramos insercion si es llave repetida
	}

	private static NodoInt revisar ( NodoInt a, double x, boolean raiz) {
		if (raiz) { //Revisa los nietos para hacer los cambios
			return revisarHijo(revisarNieto(a, x), x); //Si es la real raiz le revisa los hijos
		} else {
			return revisarNieto(a, x);
		}
	}

	private static NodoInt revisarHijo(NodoInt a, double x) { //Revisa hijos haciendo rot simple si est� x
		int i  = 1;
		int d = -1;
		if (a.izq instanceof NodoInt && ((NodoInt)a.izq).info == x) {
			return RotSimple(a,i);
		} else if (a.der instanceof NodoInt && ((NodoInt)a.der).info == x) {
			return RotSimple(a,d);
		} else return a; //Si no la encuentra se retorna as� mismo, porque �l mismo es x
	}

	private static NodoInt revisarNieto(NodoInt a, double x) { //REvisa nietos haciendo rot dobles si est� x
		int i  = 1;
		int d  =-1;
		if (a.izq instanceof NodoInt) {
			NodoInt izq = (NodoInt) a.izq;
			if (izq.izq instanceof NodoInt && ((NodoInt)izq.izq).info == x) {
				return RotZigZig(a,i);
			}
			if (izq.der instanceof NodoInt && ((NodoInt)izq.der).info == x) {
				return RotDoble(a,i);
			}
		}
		if (a.der instanceof NodoInt) {
			NodoInt der = (NodoInt) a.der;
			if (der.izq instanceof NodoInt && ((NodoInt)der.izq).info == x)	{
				return RotDoble(a,d);
			}
			if (der.der instanceof NodoInt && ((NodoInt)der.der).info == x) {
				return RotZigZig(a,d);
			}
		}
		return a; 
	}

	public static NodoInt RotSimple (NodoInt p, int sentido) //Rotaci�n simple de AVL
		{
    	if (sentido >= 1)
	  {
	  	NodoInt pp = new NodoInt( ((NodoInt) p.izq).der, p.info, p.der);
		return new NodoInt( ((NodoInt) p.izq).izq, ((NodoInt) p.izq).info, pp);
	  }
		else
	  {
	  	NodoInt pp = new NodoInt( p.izq, p.info, ((NodoInt) p.der).izq);
		return new NodoInt(pp, ((NodoInt) p.der).info, ((NodoInt) p.der).der);
	  }
    }
  
	public static NodoInt RotDoble (NodoInt p, int sentido) //Rotaci�n doble de AVL
    {
    	if (sentido >= 1)
	  {
	  	NodoInt izquierda = new NodoInt( ((NodoInt) p.izq).izq,
			((NodoInt) p.izq).info, ((NodoInt) ((NodoInt) p.izq).der).izq);
		NodoInt pp = new NodoInt( ((NodoInt) ((NodoInt) p.izq).der).der,
			p.info, p.der );
		return new NodoInt( izquierda, ((NodoInt) ((NodoInt) p.izq).der).info, pp);
	  }
		else
	  {
	  	NodoInt derecha = new NodoInt( ((NodoInt) ((NodoInt) p.der).izq).der,
			((NodoInt) p.der).info,   ((NodoInt) p.der).der );
		NodoInt pp = new NodoInt( p.izq,
			p.info, ((NodoInt) ((NodoInt) p.der).izq).izq );
		return new NodoInt( pp, ((NodoInt) ((NodoInt) p.der).izq).info, derecha);
	  }
    }

	public static NodoInt RotZigZig( NodoInt p, int sentido) {// Rotaci�n Zi(a)gZi(a)g
	 if(sentido >= 1) {
		 NodoInt izq    = (NodoInt) p.izq;
		 NodoInt izqizq = (NodoInt) izq.izq;	
			return new NodoInt(
					izqizq.izq,
					izqizq.info,
				    new NodoInt(izqizq.der, izq.info,   new NodoInt(izq.der, p.info, p.der))); 
		 
	 }else {
		 NodoInt der    = (NodoInt)p.der;
		 NodoInt derder = (NodoInt)der.der;
		 return new NodoInt(new NodoInt( new NodoInt(p.izq,p.info,der.izq),der.info, derder.izq),         
				 			derder.info,
				 			derder.der);
				 
	 }
 }
		
}