/** Algoritmo de insercion en la raiz (version corregida)
*/
public class Air
  {
	public static Nodo insertar( Nodo a, double x )
	  {
		if( a instanceof NodoExt )
		  {
			return new NodoInt( Nodo.nulo, x, Nodo.nulo );
		  }
		NodoInt b = (NodoInt)a;
		if( x<b.info )
      		  {
 			NodoInt r = new NodoInt(insertar(b.izq,x),b.info,b.der);
			NodoInt i = (NodoInt)r.izq;
			return new NodoInt(i.izq,i.info,new NodoInt(i.der,r.info,r.der));
      		  }
		else if( x>b.info )
      		  {
			NodoInt r = new NodoInt(b.izq,b.info,insertar(b.der,x));
      		NodoInt d = (NodoInt)r.der;
      		return new NodoInt(new NodoInt(r.izq,r.info,d.izq),d.info,d.der);
       		  }
		else
			return b; // ignoramos insercion si es llave repetida
	  }
  }
