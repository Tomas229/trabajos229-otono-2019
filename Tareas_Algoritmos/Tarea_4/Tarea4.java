import java.util.Scanner;
class Main {
	static PilaLista pila = new PilaLista();
	
	static String leerInput() {
		Scanner s = new Scanner(System.in);
		String Orden = s.nextLine();
		s.close();
		return Orden;
	}
	
	
	static Nodo generaArbol(String recorrido) {
		for (int i=0; i<recorrido.length(); i++) {
			if(recorrido.charAt(i)==' ') {
				
			}else {
				if(recorrido.charAt(i) == '.') {
					pila.apilar(null);
				}
				else {
					Nodo subArbDer = pila.desapilar();
					Nodo subArbIzq = pila.desapilar();
					Nodo nuevoNodo = new Nodo(recorrido.substring(i, i+1), subArbIzq, subArbDer);
					pila.apilar(nuevoNodo);
				}
			}
		}
		return pila.desapilar();
	}
	
	static String generaPreorden(Nodo raiz) {
		if(raiz == null) {
			return ".";
		}else {
			String izq = generaPreorden(raiz.izq);
			String der = generaPreorden(raiz.der);
			String raizStr = raiz.elemento;
			return raizStr + " " + izq + " " + der;
		}
	}
	
	public static void main(String[] args) {
		String input = leerInput();
		Nodo arbol = generaArbol(input);
		String output = generaPreorden(arbol);
		System.out.println(output);
	}	
}

class Nodo{
	String elemento;
	Nodo izq;
	Nodo der;
	
	Nodo(String val, Nodo izquierdo, Nodo derecho){
		elemento = val;
		izq=izquierdo;
		der=derecho;
	}
	
	Nodo(String x){
		elemento = x;
		izq= null;
		der= null;
	}
	
}


class NodoLista
{
  Nodo elemento;
  NodoLista siguiente;

  NodoLista(Nodo o)
  {
    this.elemento=o;
    this.siguiente=null;
  }

  NodoLista(Nodo o, NodoLista n)
  {
    this.elemento=o;
    this.siguiente=n;
  }
}

class PilaLista
{
  private NodoLista lista;

  public PilaLista()
  {
    lista=null;
  }

  public void apilar(Nodo x)
  {
    lista=new NodoLista(x, lista);
  }

  public Nodo desapilar() // si esta vacia se produce UNDERFLOW
  {
    if (!estaVacia())
    {
      Nodo x=lista.elemento;
      lista=lista.siguiente;
      return x;
    }
    return null;
  }

  public boolean estaVacia()
  {
    return lista==null;
  }
}