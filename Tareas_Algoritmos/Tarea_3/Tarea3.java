import java.util.Scanner;
class Main{
	static int numeroDeCursos = 0; //
	
	static nodoCurso[] leerEntrada() {
		Scanner s = new Scanner(System.in);
	
		numeroDeCursos = Integer.parseInt(s.nextLine());					
		
		nodoCurso [] listaCursos = new nodoCurso[numeroDeCursos]; 
		
		for(int i = 0; i<numeroDeCursos; i++ ) {
			String[] linea = s.nextLine().split(" ");
			
			int n = Integer.parseInt(linea[0]);      //nombre del curso
			int [] req = new int [linea.length-1];   //requisitos del curso
			
			for(int j=0; j<linea.length-1; j++) {
				req[j]=Integer.parseInt(linea[j+1]); //se llenan los requisitos	
			}
			listaCursos[i] = new nodoCurso(n,req);   //Se guardan datos 
													//en nodo curso
		}
		s.close();
		return listaCursos;
	}
	
	static Lista[] listaHeader(int n) { 	//lista que contendr� a los cursos
		Lista [] lista = new Lista [n];		//ordenados seg�n su nivel
		for(int i=0; i<n; i++) {
			lista[i] = new Lista(); 
		}
		return lista;
	}
	
	static Lista[] setUpCursos(Lista[]lista, nodoCurso[] cursos) {	//Mete cursos del input
		for( int i=0; i<cursos.length; i++) {						//a la lista  
			for(int j=0; j<lista.length; j++) {
				if(cursos[i].nivel==j) {
					IteradorLista it = new IteradorLista(lista[j].cabecera);
					lista[j].insertar(cursos[i], it);
				}
			}
		}
		return lista;
	}
	
	static Lista[] extraerCurso(Lista[]lista) {			//Extrae curso sin req 
		Lista cursosLista = lista[0];					//Y genera sus consecuencias
		IteradorLista it = new IteradorLista(cursosLista.cabecera);
		it.avanzar();
		int eliminado = it.actual.elemento.curso;
		System.out.println(eliminado);
		cursosLista.eliminar(it.obtener());
		lista = eliminarRequerimientos(lista, eliminado);
		lista = reordenarRequerimientos(lista);
		
		return lista;
	}
	
	static Lista[] eliminarRequerimientos(Lista[] lista, int n) {	//elimina un req de los cursos
		for (int i=1; i<lista.length; i++) {     //No se revisa el primero, pues no tienen requerimientos
			IteradorLista it = new IteradorLista(lista[i].cabecera);
			it.avanzar();
			while(it.actual!=null) {
				it.obtener().elimRequ(n);
				it.obtener().actualizarNivel();
				it.avanzar();
			}
			
		}
		return lista;
	}
	
	static Lista[] reordenarRequerimientos(Lista [] lista) { //reordena los cursos seg�n su nivel
		for(int i=1; i<lista.length; i++) {		//No se cambia el primero, pues no tienen requerimientos
			IteradorLista it = new IteradorLista(lista[i].cabecera);  //Puntero al header
			it.avanzar();
			while(it.actual!=null) {									// Avanza en la lista
				nodoCurso nodo = it.obtener();							
				if(nodo.getNivel()!=i) {								// revisa que est� en su lugar correspondiente
					lista[i].eliminar(nodo);							// Si no es su lugar, lo cambia
					IteradorLista it2 = new IteradorLista(lista[nodo.getNivel()].cabecera);
					lista[nodo.getNivel()].insertar(nodo, it2);
				}
				it.avanzar();
			}
		}
		return lista;
	}
	
	static void cursosPorOrden() { 	//Funci�n maestra
		nodoCurso [] listaDeCursos = leerEntrada();
		
		Lista [] listaOrdenada = listaHeader(listaDeCursos.length);
		numeroDeCursos = 7;
		listaOrdenada = setUpCursos(listaOrdenada, listaDeCursos);	
	
		for(int l=0; l<numeroDeCursos;l++) {
			listaOrdenada = extraerCurso(listaOrdenada);
		}
		
	}
	
		
	public static void main(String[] args) {
			cursosPorOrden();
		}	
}
	


class nodoCurso{
		int curso;		
		int[] requisitos;
		int nivel;			//n�mero de requisitos de un curso
		
		nodoCurso(int num, int[]req){	
			this.curso= num;
			this.requisitos = req;
			this.nivel = this.cuentaReq();
		}
		
		int cuentaReq() {		//cuenta los requisitos de un curso
			if(this.requisitos == null) {
				return 0;
			}
			
			int contador = 0;
			for(int i=0; i<this.requisitos.length; i++) {
				if (this.requisitos[i]!=0) {
					contador += 1;
				}
			}
			return contador;
		}
		
		void elimRequ(int n) {	//elimina un curso de los requisitos
			for(int i=0; i<this.requisitos.length; i++) {
				if (this.requisitos[i]==n) {
					this.requisitos[i]=0;
				}
			}
		}
		
		void actualizarNivel() {
			this.nivel=this.cuentaReq();
		}
		
		int getNivel() {
			return this.nivel;
		}
			
	}

	//Clases del apunte
class NodoLista{
	nodoCurso elemento;
	NodoLista siguiente;
	
	NodoLista(nodoCurso o){
		this.elemento = o;
		this.siguiente = null;
	}
	
	NodoLista(nodoCurso o, NodoLista n){
		this.elemento = o;
		this.siguiente = n;
	}
}


class IteradorLista
{
  NodoLista actual;

  IteradorLista(NodoLista n)
  {
    this.actual=n;
  }

  boolean estaFuera()
  {
    if (actual==null)
      return true;
    else
      return false;
  }

  nodoCurso obtener()
  {
    return this.actual.elemento;
  }

  void avanzar()
  {
    if(!estaFuera())
    {
      this.actual=this.actual.siguiente;
    }
  }
}


class Lista
{
  NodoLista cabecera;

  Lista()
  {
    this.cabecera=new NodoLista(null);
  }

  boolean estaVacia()
  {
    if (this.cabecera.siguiente==null)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  IteradorLista buscar(Object o)
  {
    NodoLista nodo=this.cabecera.siguiente;
    while (nodo!=null && !nodo.elemento.equals(o))
    {
      nodo=nodo.siguiente;
    }
    return new IteradorLista(nodo);
  }

  void insertar(nodoCurso o, IteradorLista it)
  {
    if (it!=null && it.actual!=null)
    {
      it.actual.siguiente=new NodoLista(o, it.actual.siguiente);
    }
  }

  void eliminar(nodoCurso o)
  {
    IteradorLista it=encontrarPrevio(o);
    if (it.actual.siguiente!=null)
    {
      it.actual.siguiente=it.actual.siguiente.siguiente;
    }
  }

  IteradorLista encontrarPrevio(nodoCurso o)
  {
    NodoLista nodo=this.cabecera;
    while (nodo.siguiente!=null && !nodo.siguiente.elemento.equals(o))
    {
      nodo=nodo.siguiente;
    }
    return new IteradorLista(nodo);
  }
}