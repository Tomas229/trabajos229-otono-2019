import java.util.Scanner;

public class Koch {
	//Par�metros de inicio de la tortuga 
	//Se eligieron estos para que quedase un poco m�s centrado para el largo elegido
	static int x0turtle=292;
	static int y0turtle=400;
	static int a0turtle=0;
	static int anchoCanvas=800;
	static int altoCanvas=800;
	
	// largo m�nimo
	static int  LMIN = 8;
	// Tortuga 
	static Turtle t= new Turtle(x0turtle,y0turtle,a0turtle,anchoCanvas,altoCanvas);
	// largo del copo
	static int Largo=216;
	
	public static void curvaDeKoch(Turtle tortuga, int largo) {// Se encarga de crear una curva de Koch
		if (largo <= LMIN)
			tortuga.goForward(LMIN);
		else {
			curvaDeKoch(tortuga,largo/3);
			tortuga.turnRight(60);
			curvaDeKoch(tortuga,largo/3);
			tortuga.turnLeft(120);
			curvaDeKoch(tortuga,largo/3);
			tortuga.turnRight(60);
			curvaDeKoch(tortuga,largo/3);
		}
	}
	
	
	
	public static void main (String[] args) { //Main, pide al usuario �ngulo de rotaci�n y dibuja copo de Koch
		System.out.println("Seleccione un grado de rotaci�n para su copo:"); 
		Scanner S = new Scanner(System.in);
        int grado = Integer.parseInt(S.nextLine()); //Se espera solamente enteros
        
        t.turnLeft(grado); //primero gira los grados entregados por el usuario 
        
        //Dibujo de las tres curvas del copo de Koch
		curvaDeKoch(t,Largo);
		t.turnLeft(120);
		curvaDeKoch(t,Largo);
		t.turnLeft(120);
		curvaDeKoch(t,Largo);
		t.turnLeft(120);
		System.out.println("Done"); // Para indicar que se debe ver la pantalla del dibujo
	}

	
}
