def cantidad(n):
    if n == 1:
        return 4
    
    if n == 2 or n == 3 or n == 4:
        return 0
    
    
    sn1 = 0
    sn2 = 0
    k1 = 1
    k2 = 1
        
    while k1 < n-3:
        sn1 += cantidad(k1) * cantidad(n- k1 -3)
        k1 += 1
            
    while k2 < n- 4:
        sn2 += cantidad(k2) * cantidad(n- k2- 4)
        k2 += 1
        
   
    return 2* sn1 + sn2


def esExp(exp):
    if cantidad(len(exp)) == 0:                             #Verifica largo válido
        return False

    elif exp == "a" or exp == "b" or exp == "1" or exp == "2":      #Regla base
            return True

    else: 
        if exp.startswith("(") == False or exp.endswith(")") == False:  #Verifica que esté entre paréntesis
            return False
        else: 
            exp = exp[1:len(exp)-1]                                     #Quita los paréntesis 
            
            if esExp(exp[0]):           #Primer carácter es una expresión
                regla23 = (exp[1] == "*" or exp[1] == "+") and esExp(exp[2:])   #regla  2 o regla 3 y que lo otro sea expresión
                regla4  =  exp[1] == "=" and exp[2] == "=" and esExp(exp[3:])   #Regla 4 y que lo otro sea expresión
                return regla23 or regla4
            
            elif exp[0] == "(":           #Primer carácter es paréntesis
                cuenta = 0                                              #Contador de paréntesis
                indice = 0                                              #Indica cierre del paréntesis
                for i in exp:
                    if i == "(":                                        #Cuando se abre un paréntesis suma 1
                        cuenta += 1
                        
                    elif i == ")":                                      #Cuando se cierra un paréntesis resta 1
                        cuenta -= 1 
                        
                    if cuenta == 0:                                     #Se cerraron todos los paréntesis, por lo que termina la expresión izq
                        break
                        
                    
                    indice += 1
                    
                if cuenta != 0:                                         #No se cerraron los paréntesis, por lo que no es válido
                    return False
                    
                if len(exp[indice + 1:]) < 2:                  #No hay suficiente espacio para más expresiones, no válido
                    return False
                    
                elif len(exp[indice + 1: len(exp)]) == 2:               #Solamente hay espacio para regla 23
                    regla23 = esExp(exp[0:indice+1]) and (exp[indice +1] == "*" or exp[indice +1] == "+") and esExp(exp[indice + 2:])
                    return regla23
                    
                elif len(exp[indice +1: len(exp)]) == 3:                  #Solamente hay espacio para regla 4
                    regla4 =  esExp(exp[0:indice+1]) and  exp[indice +1] == "=" and exp[indice +2] == "=" and esExp(exp[indice + 3:])
                    return regla4
                    
                else:                                                    #Hay espacio para ambas posibilidades
                    regla23 = esExp(exp[0:indice+1]) and (exp[indice +1] == "*" or exp[indice +1] == "+") and esExp(exp[indice + 2:])
                    regla4  = esExp(exp[0:indice+1]) and  exp[indice +1] == "=" and exp[indice +2] == "=" and esExp(exp[indice + 3:])
                    return regla23 or regla4
                
            else:                         #Primer carácter es no válido
                return False
                
            
            
          
            
            

i = input()
a = esExp(i)
b = cantidad(len(i))
if a == True:
    a = 1
elif a == False:
    a=  0
    
print(str(a) + " " + str(b))
    
    