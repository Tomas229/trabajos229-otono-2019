def revisarPalabra(str, dic):       #Revisa que una palabra esté

    for i in range(len(dic)):
        if str.find(dic[i]) != -1:
            return i
        
    return -1

def revisarPalabra2(str, dic):     #Entrega cuál e el índice de la palabra

    for palabra in dic:
        if str.find(palabra) != -1:
            return True
    
    return False


k=int(input())             #Se obtiene el input

miString = "".zfill(k)     #Se crea la palabra base

diccionario = []           #Se crea el diccionario
 
lista = range(1,2**k)      #Se ignora el 0000... porque partiremos con ese

for x in  lista:            #Se rellena el diccionario
    diccionario += [str(bin(x)[2:]).zfill(k)]  
    



while(len(diccionario)>0):
    
    if revisarPalabra2(miString + "1",diccionario):     #Se agrega uno si este agrega una secuencia del diccionario
        miString += "1"
        del diccionario[revisarPalabra(miString,diccionario)]
    else:
        miString += "0"                                 #En caso contrario le agrega un 0
        del diccionario[revisarPalabra(miString,diccionario)]
  
print(miString)

#print(len(miString)==(2**k + k -1))   #Verificar que sea del largo adecuado