import sys
num = int(input())          #Input inicial
lista =[]

for line in sys.stdin:      #Lista de velocidades
        lista += [int(line)]
        num -=1
        if num<1: break

lista.sort()                #Se ordena de menor a mayor

tiempo = 0                  #Se inicializa el tiempo
    
while(len(lista)>3):        #Caso general
    tiempo += min(lista[0] + 2*lista[1] + lista[len(lista)-1],
                  2*lista[0] + lista[len(lista)-1]+ lista[len(lista)-2])
    lista = lista[:len(lista)-2]
    
if(len(lista)==3):          #Caso base
    tiempo += lista[0] + lista[1] + lista[2]
    
if(len(lista)==2):          #Caso base
    tiempo+= lista[1]
    
print(tiempo)