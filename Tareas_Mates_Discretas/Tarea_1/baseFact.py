import math as m 

def obtenerIndices(n,mayorFactor):
    if n == 0:
        return 0

    for i in range(mayorFactor + 1):
        if i*m.factorial(mayorFactor) > n:
            return  i - 1
        elif i*m.factorial(mayorFactor) == n:
            return  i
    
    return i
   
    
def baseFact(n):
    k = 1
    mayorFactorFijo = 0
    mayorFactor = 0
    listaBase = []
    
    while m.factorial(k) < n:
        k += 1
    else:
        if m.factorial(k)==n:
            mayorFactor = k
            mayorFactorFijo = k
        else:
            mayorFactor = k -1
            mayorFactorFijo = k - 1
    
    for i in range(mayorFactorFijo + 1):
        indice = obtenerIndices(n,mayorFactor)
        listaBase += [indice]
        n = n - indice * m.factorial(mayorFactor)
        mayorFactor = mayorFactor - 1
          
    
    
    return listaBase 
    
assert(baseFact(463) == [3,4,1,0,1,0])

print('#'.join(map(str, baseFact(int(input())))))