import math as m 

def obtenerFib(n):   #entrega la el número de fibonacci n
    phipos = (1 + m.sqrt(5))/2
    phineg = (1 - m.sqrt(5))/2
    fib= int((1/m.sqrt(5))*(phipos**n-phineg**n))
    return fib
   
def mayorFactorFib(n): #obtiene el número de fib más grande que es menor a n
    k = 0
    while obtenerFib(k) < n:
        k += 1
    else:
        if obtenerFib(k)==n:
            return k
        else:
            return k - 1
    return None
    
def obtenerIndices(n,mayorFactor,valor): #genera los índices para la lista 
    if valor != 0:
        return 0
    else:
        if obtenerFib(mayorFactor)>n:
            return 0
        else:
            return 1
    return 1
    
def listToFib(lista):  #convierte de lista de índdices a número de fib (Necesario para comprobar que esté bien)
    numero = 0
    for i in range(len(lista)):
        numero += obtenerFib(len(lista)+1-i)*lista[i]
    
    return numero
    
def baseFib(n):
    mayorFactorFijo = mayorFactorFib(n)
    mayorFactor = mayorFactorFib(n)
    listaBase = [0]
    
    if n == 0:
        return listaBase
    elif n ==1:
        return [1]
    
    
    for i in range(mayorFactorFijo - 1):
        indice = obtenerIndices(n,mayorFactor,listaBase[i])
        listaBase += [indice]
        if indice != 0:
            n = n - obtenerFib(mayorFactor)
        mayorFactor = mayorFactor - 1
          
    listaBase.pop(0)
    return listaBase 

assert(baseFib(64) == [1,0,0,0,1,0,0,0,1])

print('#'.join(map(str, baseFib(int(input())))))