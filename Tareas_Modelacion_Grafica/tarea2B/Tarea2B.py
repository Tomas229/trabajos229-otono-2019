# coding=utf-8

import glfw
from OpenGL.GL import *
import OpenGL.GL.shaders
import numpy as np
import sys

import transformations2 as tr2
import basic_shapes as bs
import scene_graph2 as sg
import easy_shaders as es
import catrom as cr
import basic_shapes_extended as bse


# A class to store the application control
# Add follow_car option
class Controller:
    def __init__(self):
        self.fillPolygon = True
        self.showAxis = False
        self.camNumber = 1
        self.helicopter = False

# we will use the global controller as communication with the callback function
controller = Controller()


def on_key(window, key, scancode, action, mods):

    if action != glfw.PRESS:
        return
    
    global controller

    if key == glfw.KEY_SPACE:
        controller.fillPolygon = not controller.fillPolygon

    elif key == glfw.KEY_LEFT_CONTROL:
        controller.showAxis = not controller.showAxis

    elif key == glfw.KEY_ESCAPE:
        sys.exit()
        
    elif key == glfw.KEY_1:
        controller.camNumber = 1
        controller.helicopter = False
        
    elif key == glfw.KEY_2:
        controller.camNumber = 2
        controller.helicopter = False
        
    elif key == glfw.KEY_3:
        controller.camNumber = 3
        controller.helicopter = False
    
    elif key == glfw.KEY_4:
        controller.camNumber = 4
        controller.helicopter = False
       
    elif key == glfw.KEY_5:
        controller.helicopter = not controller.helicopter
        
    elif (key == glfw.KEY_UP or key == glfw.KEY_DOWN or key == glfw.KEY_LEFT or key == glfw.KEY_RIGHT):
        print("Directional input")
        #This is to avoid the else instruction
        
    else:
        print('Unknown key')
# Create world and building with textures
   
def createWorld():
    gpuGround_texture = es.toGPUShape(bs.createTextureQuad("ground.png"), GL_REPEAT, GL_NEAREST)
    gpuNorth          = es.toGPUShape(bs.createTextureQuad("enviroment.jpg"), GL_REPEAT, GL_NEAREST)
    gpuSouth          = es.toGPUShape(bs.createTextureQuad("enviroment.jpg"), GL_REPEAT, GL_NEAREST)
    gpuEast           = es.toGPUShape(bs.createTextureQuad("enviroment.jpg"), GL_REPEAT, GL_NEAREST)
    gpuWest           = es.toGPUShape(bs.createTextureQuad("enviroment.jpg"), GL_REPEAT, GL_NEAREST)
    gpuSky            = es.toGPUShape(bs.createTextureQuad("sky.png"), GL_REPEAT, GL_NEAREST)
    
    
    ground_scaled = sg.SceneGraphNode("ground_scaled")
    ground_scaled.transform = tr2.scale(2, 2, 2)
    ground_scaled.childs += [gpuGround_texture]

    ground_rotated = sg.SceneGraphNode("ground_rotated_x")
    ground_rotated.transform = tr2.matmul([tr2.rotationX(np.pi),tr2.rotationZ(np.pi)])
    ground_rotated.childs += [ground_scaled]

    ground = sg.SceneGraphNode("ground")
    ground.transform = tr2.translate(0, 0, 0)
    ground.childs += [ground_rotated]
    
    northScaled = sg.SceneGraphNode("northScaled")
    northScaled.transform = tr2.scale(2,4,1)
    northScaled.childs += [gpuNorth]
    
    northRotated = sg.SceneGraphNode("northRotated")
    northRotated.transform = tr2.matmul([tr2.rotationX(-np.pi*0.5),tr2.rotationZ(np.pi)])
    northRotated.childs += [northScaled]
    
    northTranslated = sg.SceneGraphNode("northTranslated")
    northTranslated.transform = tr2.translate(0,1,2)
    northTranslated.childs += [northRotated]
    
    southScaled = sg.SceneGraphNode("southScaled")
    southScaled.transform = tr2.scale(2,4,1)
    southScaled.childs += [gpuSouth]
    
    southRotated = sg.SceneGraphNode("southRotated")
    southRotated.transform = tr2.rotationX(np.pi*0.5)
    southRotated.childs += [southScaled]
    
    southTranslated = sg.SceneGraphNode("southTranslated")
    southTranslated.transform = tr2.translate(0,-1,2)
    southTranslated.childs += [southRotated]
   
    eastScaled = sg.SceneGraphNode("eastScaled")
    eastScaled.transform = tr2.scale(2,4,1)
    eastScaled.childs += [gpuEast]
    
    eastRotated = sg.SceneGraphNode("eastRotated")
    eastRotated.transform = tr2.matmul([tr2.rotationX(np.pi*0.5),tr2.rotationY(np.pi*0.5)])
    eastRotated.childs += [eastScaled]
    
    eastTranslated = sg.SceneGraphNode("eastTranslated")
    eastTranslated.transform = tr2.translate(1,0,2)
    eastTranslated.childs += [eastRotated]
    
    westScaled = sg.SceneGraphNode("westScaled")
    westScaled.transform = tr2.scale(2,4,1)
    westScaled.childs += [gpuWest]
    
    westRotated = sg.SceneGraphNode("westRotated")
    westRotated.transform = tr2.matmul([tr2.rotationX(np.pi*0.5),tr2.rotationY(np.pi*0.5)])
    westRotated.childs += [westScaled]
    
    westTranslated = sg.SceneGraphNode("westTranslated")
    westTranslated.transform = tr2.translate(-1,0,2)
    westTranslated.childs += [westRotated]
    
    skyScaled = sg.SceneGraphNode("skyScaled")
    skyScaled.transform = tr2.scale(2, 2, 2)
    skyScaled.childs += [gpuSky]

    skyRotated = sg.SceneGraphNode("skyRotated")
    skyRotated.transform = tr2.rotationX(0)
    skyRotated.childs += [skyScaled]

    skyTranslated = sg.SceneGraphNode("skyTranslated")
    skyTranslated.transform = tr2.translate(0, 0, 4)
    skyTranslated.childs += [skyRotated]
    
    world = sg.SceneGraphNode("world")
    world.transform = tr2.identity()
    world.childs += [ground]
    world.childs += [northTranslated]
    world.childs += [southTranslated]
    world.childs += [eastTranslated]
    world.childs += [westTranslated]
    world.childs += [skyTranslated]
    
    return world

def createBuilding():
    dim = 0.25
    gpuTowerTexture1 = es.toGPUShape(bs.createTextureTriangularPrism("BankOfChinaWall.png", 1, 1), GL_REPEAT, GL_NEAREST)
    gpuTowerTexture2 = es.toGPUShape(bs.createTextureTriangularPrism("BankOfChinaWall.png", 1, 2), GL_REPEAT, GL_NEAREST)
    gpuTowerTexture3 = es.toGPUShape(bs.createTextureTriangularPrism("BankOfChinaWall.png", 1, 3), GL_REPEAT, GL_NEAREST)
    gpuTowerTexture5 = es.toGPUShape(bs.createTextureTriangularPrism("BankOfChinaWall.png", 1, 5), GL_REPEAT, GL_NEAREST)
    gpuRoofTexture   = es.toGPUShape(bs.createTexturePiramid("Roof.png",1,1), GL_REPEAT, GL_NEAREST)
    gpuEntranceHall  = es.toGPUShape(bs.createTextureCube("entranceHall.png"), GL_REPEAT, GL_NEAREST)
    gpuEntranceDoor  = es.toGPUShape(bs.createTextureQuad("EntranceHall2.png",1,1),GL_REPEAT, GL_NEAREST)
    
    entranceDoor = sg.SceneGraphNode("entranceDoor")
    entranceDoor.transform = tr2.scale(0.5,0.25,1)
    entranceDoor.childs += [gpuEntranceDoor]
    
    entranceDoorRotated = sg.SceneGraphNode("entranceDoorRotated")
    entranceDoorRotated.transform = tr2.rotationX(np.pi*0.5)
    entranceDoorRotated.childs += [entranceDoor]
    
    entranceDoorTranslated = sg.SceneGraphNode("entranceDoorTranslated")
    entranceDoorTranslated.transform = tr2.translate(0,-dim-0.0001,dim*0.25)
    entranceDoorTranslated.childs += [entranceDoorRotated]
    
    entranceHall = sg.SceneGraphNode("entranceHall")
    entranceHall.transform = tr2.matmul([tr2.translate(0,0,dim*0.25),tr2.scale(0.5,0.5,0.125)])
    entranceHall.childs += [gpuEntranceHall]
   
    groundFloor = sg.SceneGraphNode("groundFloor")
    groundFloor.transform = tr2.identity()
    groundFloor.childs += [entranceHall]
    groundFloor.childs += [entranceDoorTranslated]
   
    
    baseRoof = sg.SceneGraphNode("baseRoof")
    baseRoof.transform = tr2.identity()
    baseRoof.childs += [gpuRoofTexture]
    
    roof1 = sg.SceneGraphNode("roof1")
    roof1.transform = tr2.translate(0, 0, dim*2)
    roof1.childs += [baseRoof]
    
    roof2 = sg.SceneGraphNode("roof2")
    roof2.transform = tr2.translate(0, 0, 2*dim*2)
    roof2.childs += [baseRoof]
    
    roof3 = sg.SceneGraphNode("roof3")
    roof3.transform = tr2.translate(0, 0, 3*dim*2)
    roof3.childs += [baseRoof]
    
    roof5 = sg.SceneGraphNode("roof5")
    roof5.transform = tr2.translate(0, 0, 5*dim*2)
    roof5.childs += [baseRoof]
    
    baseTower1 = sg.SceneGraphNode("baseTower1")
    baseTower1.transform = tr2.identity()
    baseTower1.childs += [gpuTowerTexture1]
    
    baseTower2 = sg.SceneGraphNode("baseTower2")
    baseTower2.transform = tr2.identity()
    baseTower2.childs += [gpuTowerTexture2]
    
    baseTower3 = sg.SceneGraphNode("baseTower3")
    baseTower3.transform = tr2.identity()
    baseTower3.childs += [gpuTowerTexture3]
    
    baseTower5 = sg.SceneGraphNode("baseTower5")
    baseTower5.transform = tr2.identity()
    baseTower5.childs += [gpuTowerTexture5]
    
    tower1Translated = sg.SceneGraphNode("tower1Translated")
    tower1Translated.transform = tr2.translate(0, -dim, dim*0.5)
    tower1Translated.childs += [baseTower1]
    tower1Translated.childs += [roof1]
    
    tower2Scaled = sg.SceneGraphNode("tower2Scaled")
    tower2Scaled.transform = tr2.scale(1, 1, 3)
    tower2Scaled.childs += [baseTower3]
    
    tower2Rotated = sg.SceneGraphNode("tower2Rotated")
    tower2Rotated.transform = tr2.rotationZ(-np.pi*0.5)
    tower2Rotated.childs += [tower2Scaled]
    tower2Rotated.childs += [roof3]
    
    tower2Translated = sg.SceneGraphNode("tower2Translated")
    tower2Translated.transform = tr2.translate(-dim, 0, dim*0.5)
    tower2Translated.childs += [tower2Rotated]
    
    tower3Scaled = sg.SceneGraphNode("tower3Scaled")
    tower3Scaled.transform = tr2.scale(1, 1, 5)
    tower3Scaled.childs += [baseTower5]
    
    tower3Rotated = sg.SceneGraphNode("tower3Rotated")
    tower3Rotated.transform = tr2.rotationZ(-np.pi)
    tower3Rotated.childs += [tower3Scaled]
    tower3Rotated.childs += [roof5]
    
    tower3Translated = sg.SceneGraphNode("tower3Translated")
    tower3Translated.transform = tr2.translate(0, dim, dim*0.5)
    tower3Translated.childs += [tower3Rotated]
    
    tower4Scaled = sg.SceneGraphNode("tower4Scaled")
    tower4Scaled.transform = tr2.scale(1, 1, 2)
    tower4Scaled.childs += [baseTower2]
    
    tower4Rotated = sg.SceneGraphNode("tower4Rotated")
    tower4Rotated.transform = tr2.rotationZ(np.pi*0.5)
    tower4Rotated.childs += [tower4Scaled]
    tower4Rotated.childs += [roof2]
    
    tower4Translated = sg.SceneGraphNode("tower4Translated")
    tower4Translated.transform = tr2.translate(dim, 0, dim*0.5)
    tower4Translated.childs += [tower4Rotated]
    
    
    
    building = sg.SceneGraphNode("building")
    building.transform = tr2.identity()
    building.childs += [tower1Translated]
    building.childs += [tower2Translated]
    building.childs += [tower3Translated]
    building.childs += [tower4Translated]
    building.childs += [groundFloor]
    
    return building
    
if __name__ == "__main__":

    # Initialize glfw
    if not glfw.init():
        sys.exit()

    width = 1000
    height = 1000

    window = glfw.create_window(width, height, "Bank of China Tower, Honk Kong, China", None, None)

    if not window:
        glfw.terminate()
        sys.exit()

    glfw.make_context_current(window)

    # Connecting the callback function 'on_key' to handle keyboard events
    glfw.set_key_callback(window, on_key)

    # Assembling the shader program (pipeline) with shaders (simple, texture and lights)
    mvcPipeline = es.SimpleModelViewProjectionShaderProgram()
    textureShaderProgram = es.SimpleTextureModelViewProjectionShaderProgram()
  


    # Setting up the clear screen color
    glClearColor(1, 1, 1, 1.0)

    # As we work in 3D, we need to check which part is in front,
    # and which one is at the back
    glEnable(GL_DEPTH_TEST)

    # Creating shapes on GPU memory
    gpuAxis = es.toGPUShape(bs.createAxis(7))
    worldNode = createWorld()
    buildingNode = createBuilding()

    
    # Create one werid object (awning, I think)
    vertices = [[1, 0], [0.9, 0.4], [0.5, 0.5], [0, 0.5], [-0.5, 0.5], [-0.9, 0.4], [-1, 0]]
    curve = cr.getSplineFixed(vertices, 10)

    obj_planeL = bse.createTexturePlaneFromCurve(curve, False, "redVelvet.jpg", center=(0, 0))
    obj_planeL.uniformScale(0.25)
    obj_planeL.translate(0,0.25, 0.25)
    obj_planeL.setShader(textureShaderProgram)
    
    # lookAt of normal camera
    """ normal_view = tr2.lookAt(
            np.array([2, 2, 2]),
            np.array([0, 0, 0]),
            np.array([0, 0, 1])
        )"""

        
    t0 = glfw.get_time()
    camera_theta = np.pi / 4
    cameraZ = 1
     
    
    
    while not glfw.window_should_close(window):

        # Telling OpenGL to use our shader program
        glUseProgram(mvcPipeline.shaderProgram)
        # Using the same view and projection matrices in the whole application
        projection = tr2.perspective(45, float(width) / float(height), 0.1, 100) 
        glUniformMatrix4fv(glGetUniformLocation(mvcPipeline.shaderProgram, "projection"), 1, GL_TRUE, projection)

        t1 = glfw.get_time()
        dt = t1 - t0
        t0 = t1
        
        eyeChangingPos = [0,0,0]
        atChangingPos  = [0,0,0]

            # static camera
        if controller.camNumber == 1:
            viewChangingPos = [0, -1, 0.047]
            atChangingPos   = [0, 0, 1]
            
            normal_view = tr2.lookAt(
                np.array(viewChangingPos),
                np.array(atChangingPos),
                np.array([0, 0, 1])
            )
            
        elif controller.camNumber == 2:
            viewChangingPos = [-0.3, -0.6, 1.45]
            atChangingPos   = [0, 0, 0]
        
            normal_view = tr2.lookAt(
                np.array(viewChangingPos),
                np.array(atChangingPos),
                np.array([0, 0, 1])
            )
            
        elif controller.camNumber == 3:
            viewChangingPos = [0.3, 0.6, 1.45]
            atChangingPos   = [0, 0, 0]
        
            normal_view = tr2.lookAt(
                np.array(viewChangingPos),
                np.array(atChangingPos),
                np.array([0, 0, 1])
            )
            
        elif controller.camNumber == 4:
            viewChangingPos = [0.7, -0.7, 3]
            atChangingPos   = [0, 0, 0]
        
            normal_view = tr2.lookAt(
                np.array(viewChangingPos),
                np.array(atChangingPos),
                np.array([0, 0, 1])
            )    
            
            
        if (glfw.get_key(window, glfw.KEY_LEFT) == glfw.PRESS
            and controller.helicopter == True):
            camera_theta -= 2 * -dt

        if (glfw.get_key(window, glfw.KEY_RIGHT) == glfw.PRESS
            and controller.helicopter == True):
            camera_theta += 2* -dt
        
        if (glfw.get_key(window, glfw.KEY_UP) == glfw.PRESS
            and controller.helicopter == True):
            if(cameraZ < 4):
                cameraZ += 1* dt
            
        if(glfw.get_key(window, glfw.KEY_DOWN) == glfw.PRESS
            and controller.helicopter == True):
            if(cameraZ > -0.03):
                cameraZ -= 1* dt
            
        if(controller.helicopter == True):
            camX = np.sin(camera_theta)
            camY = np.cos(camera_theta)
            camZ = cameraZ
        
        
            viewPos = [camX, camY, camZ]
        
            atChangingPos = [0, 0, 1.5]
        
            normal_view = tr2.lookAt(
            np.array(viewPos),
            np.array(atChangingPos),
            np.array([0,0,1]))
        
        
            

        glUniformMatrix4fv(glGetUniformLocation(mvcPipeline.shaderProgram, "view"), 1, GL_TRUE, normal_view)

        # Using GLFW to check for input events
        glfw.poll_events()

        # Clearing the screen in both, color and depth
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        # Filling or not the shapes depending on the controller state
        if (controller.fillPolygon):
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        else:
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)

        if controller.showAxis:
            glUniformMatrix4fv(glGetUniformLocation(mvcPipeline.shaderProgram, "model"), 1, GL_TRUE, tr2.identity())
            mvcPipeline.drawShape(gpuAxis, GL_LINES)

        # Drawing ground and ricardo using texture shader
        glUseProgram(textureShaderProgram.shaderProgram)
        glUniformMatrix4fv(glGetUniformLocation(textureShaderProgram.shaderProgram, "projection"), 1, GL_TRUE, projection)
        glUniformMatrix4fv(glGetUniformLocation(textureShaderProgram.shaderProgram, "view"), 1, GL_TRUE, normal_view)
        
        sg.drawSceneGraphNode(buildingNode, textureShaderProgram)
        # Drawing world
        sg.drawSceneGraphNode(worldNode, textureShaderProgram)
        #Drawin the werido
        obj_planeL.draw(normal_view, projection)


        # Once the render is done, buffers are swapped, showing only the complete scene.
        glfw.swap_buffers(window)

    glfw.terminate()