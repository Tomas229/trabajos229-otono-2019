# coding=utf-8
"""
Daniel Calderon, CC3501, 2019-1
Drawing a car via a scene graph
"""

import glfw
from OpenGL.GL import *
import OpenGL.GL.shaders
import numpy as np
import transformations as tr
import scene_graph as sg
import sys

# We will use 32 bits data, so an integer has 4 bytes
# 1 byte = 8 bits
INT_BYTES = 4


# A class to store the application control
class Controller:
    fillPolygon = True


# we will use the global controller as communication with the callback function
controller = Controller()


def on_key(window, key, scancode, action, mods):

    if action != glfw.PRESS:
        return
    
    global controller

    if key == glfw.KEY_ESCAPE:
        sys.exit()

    else:
        print('Unknown key')


def createQuad(r, g, b, r2, g2, b2):

    # Here the new shape will be stored
    gpuShape = sg.GPUShape()

    # Defining locations and colors for each vertex of the shape    
    vertexData = np.array([
    #   positions        colors
        -0.5, -0.5, 0.0,  r, g, b,
         0.5, -0.5, 0.0,  r, g, b,
         0.5,  0.5, 0.0,  r2, g2, b2,
        -0.5,  0.5, 0.0,  r2, g2, b2
    # It is important to use 32 bits data
        ], dtype = np.float32)

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = np.array(
        [0, 1, 2,
         2, 3, 0], dtype= np.uint32)

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    # Vertex data must be attached to a Vertex Buffer Object (VBO)
    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    # Connections among vertices are stored in the Elements Buffer Object (EBO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape

	
def createTriangle(r,g,b):

    # Here the new shape will be stored
    gpuShape = sg.GPUShape()

    # Defining the location and colors of each vertex  of the shape
    vertexData = np.array([
    #     positions       colors
         -0.7, -0.7, 0.0,  r, g, b,
          0.7, -0.7, 0.0,  r, g, b,
          0.0,  0.7, 0.0,  r, g, b]
          ,dtype = np.float32) # It is important to use 32 bits data

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = np.array(
        [0, 1, 2], dtype= np.uint32)
        
    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape


def createTree():
	
    log = sg.SceneGraphNode("log")
    log.transform = tr.scale(0.1,0.4,0.1)
    log.childs += [createQuad(0.4,0.2,0.21,0.4,0.2,0.21)]
	
    top = sg.SceneGraphNode("top")
    top.transform = tr.scale(0.35,0.5,0.5)
    top.childs += [createTriangle(0,0.3,0)]
	
    treeTop = sg.SceneGraphNode("treeTop")
    treeTop.transform = tr.translate(0.0,0.35,0.0)
    treeTop.childs += [top]
	
    tree = sg.SceneGraphNode("tree")
    tree.childs += [log]
    tree.childs += [treeTop]

    tree1 = sg.SceneGraphNode("tree1")
    tree1.transform = tr.scale(1,1.5,0)
    tree1.childs += [tree]

    tree1Translated = sg.SceneGraphNode("tree1Translated")
    tree1Translated.transform = tr.translate(-0.7,-0.3,0)
    tree1Translated.childs += [tree1]
	

    tree2 = sg.SceneGraphNode("tree2")
    tree2.transform = tr.scale(1,1,0)
    tree2.childs += [tree]

    tree2Translated = sg.SceneGraphNode("tree2Translated")
    tree2Translated.transform = tr.translate(+0.6,-0.3,0)
    tree2Translated.childs += [tree2]

    tree3 = sg.SceneGraphNode("tree3")
    tree3.transform = tr.scale(1,1.5,0)
    tree3.childs += [tree]

    tree3Translated = sg.SceneGraphNode("tree3Translated")
    tree3Translated.transform = tr.translate(0,0.1,0)
    tree3Translated.childs += [tree3]

    sky = sg.SceneGraphNode("sky")
    sky.transform = tr.scale(2,2,2)
    sky.childs += [createQuad(1,1,1,0,0.8,0.8)]

    grass = sg.SceneGraphNode("grass")
    grass.transform = tr.scale(2,1,0)
    grass.childs += [createQuad(0.4,0.2,0.21,0,0.4,0)]

    grassTranslated = sg.SceneGraphNode("grassTranslated")
    grassTranslated.transform = tr.translate(0,-0.5,0)
    grassTranslated.childs += [grass]
	
    Forest = sg.SceneGraphNode("Forest")
    Forest.transform = tr.identity()
    Forest.childs += [sky]
    Forest.childs += [grassTranslated]
    Forest.childs += [tree1Translated]
    Forest.childs += [tree2Translated]
    Forest.childs += [tree3Translated]
    
	
    return Forest
	
if __name__ == "__main__":

    # Initialize glfw
    if not glfw.init():
        sys.exit()

    width = 600
    height = 600

    window = glfw.create_window(width, height, "Welcome to Viridian Forest", None, None)

    if not window:
        glfw.terminate()
        sys.exit()

    glfw.make_context_current(window)

    # Connecting the callback function 'on_key' to handle keyboard events
    glfw.set_key_callback(window, on_key)

    # Assembling the shader program (pipeline) with both shaders
    shaderProgram = sg.basicShaderProgram()
    
    # Telling OpenGL to use our shader program
    glUseProgram(shaderProgram)

    # Setting up the clear screen color
    glClearColor(0.85, 0.85, 0.85, 1.0)

    # Creating shapes on GPU memory
    tree = createTree()

    # Our shapes here are always fully painted
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)

    while not glfw.window_should_close(window):
        # Using GLFW to check for input events
        glfw.poll_events()

        # Clearing the screen in both, color and depth
        glClear(GL_COLOR_BUFFER_BIT)

        # Modifying only a specific node in the scene graph

        # Drawing the Car
        sg.drawSceneGraphNode(tree, shaderProgram, tr.identity())

        # Once the render is done, buffers are swapped, showing only the complete scene.
        glfw.swap_buffers(window)

    
    glfw.terminate()
