# coding=utf-8
"""
Daniel Calderon, CC3501, 2019-1
Animation with indirect color
"""
#Se modificó ex_colo_palette_anim y
#también se utilizaron funciones de preguntas anteriores del aux1
from ex_color_palette import *


#lo que se usará para interpolar colores
def interpolacionColor(color1,color2,t):
    nuevoColor = np.array([color1[0]*(1-t)+color2[0]*t,
                           color1[1]*(1-t)+color2[1]*t,
                           color1[2]*(1-t)+color2[2]*t], dtype=np.float)
    return nuevoColor



def updatePalettegrises(colorPalette, t):
    newPalette = []

    for color in colorPalette:
        bg=(color[0]+color[1]+color[2])/3
        bgcolor=np.array([bg,bg,bg],dtype=np.float)
        newColor = interpolacionColor(color,bgcolor,t)
        newPalette += [newColor]

    return newPalette

def updatePalettesepia(colorPalette, t):
    newPalette = []

    for color in colorPalette:
        bg=(color[0]+color[1]+color[2])/3
        bgcolor=np.array([bg*0.5,bg*0.35,bg*0.2],dtype=np.float)
        newColor = interpolacionColor(color,bgcolor,t)
        newPalette += [newColor]

    return newPalette

def updatePalettenocturna(colorPalette,t):
   
    newPalette = []

    for color in colorPalette:
        v1 = 0.25
        v2 = 0.5
        v3 = 0.6
        color0 = np.array([0, 1, 0],dtype=np.float)
        color1 = np.array([0, 0, 0],dtype=np.float)
        color2 = np.array([0, 0.25, 0],dtype=np.float)
        color3 = np.array([0, 0.5, 0],dtype=np.float)
        bg=(color[0]+color[1]+color[2])/3
        if bg<=v1:
            newColor = interpolacionColor(color,color0,t)
            newPalette += [newColor]
        elif v1<bg<v2:
            newColor = interpolacionColor(color,color1,t)
            newPalette += [newColor]
        elif v2<bg<v3:
            newColor = interpolacionColor(color,color2,t)
            newPalette += [newColor]
        elif v3<=bg:
            newColor = interpolacionColor(color,color3,t)
            newPalette += [newColor]
    return newPalette


def updatePalettetermica(colorPalette,t):
   
    newPalette = []

    for color in colorPalette:
        v1 = 0.25
        v2 = 0.5
        v3 = 0.6
        color0 = np.array([1, 0, 0],dtype=np.float)
        color1 = np.array([0, 0, 1],dtype=np.float)
        color2 = np.array([1, 1, 0],dtype=np.float)
        color3 = np.array([0, 1,1],dtype=np.float)
        bg=(color[0]+color[1]+color[2])/3
        if bg<=v1:
            newColor = interpolacionColor(color,color0,t)
            newPalette += [newColor]
        elif v1<bg<v2:
            newColor = interpolacionColor(color,color1,t)
            newPalette += [newColor]
        elif v2<bg<v3:
            newColor = interpolacionColor(color,color2,t)
            newPalette += [newColor]
        elif v3<=bg:
            newColor = interpolacionColor(color,color3,t)
            newPalette += [newColor]
    return newPalette
    


if __name__ == "__main__":

    # Reading an image into a numpy array
    originalImage = mpl.imread("santiago.png")

    # Obtaining all different colors in the image and the indexed image
    indexedImage, colorPalette = getColorPalette(originalImage)

    # Reconstructing image
    animatedImage = assignColors(indexedImage, colorPalette)

    fig, ax = mpl.subplots()
    im = ax.imshow(originalImage, animated=True)

    time = 0

    def updateFig(*args):
        global time
        time += 0.1
        param = np.abs(np.sin(time))
        #Se debe cambiar el nombre del updatePalette
        #updatePalettegrises o updatePalettetermica
        # o updatePalettenocturna o updatePalettesepia
        newColorPalette = updatePalettetegrises(colorPalette, param)
        updatedImage = assignColors(indexedImage, newColorPalette)
        im.set_array(updatedImage)
        return im,

    ani = animation.FuncAnimation(fig, updateFig, interval=50, blit=True)
    mpl.show()





    
    
